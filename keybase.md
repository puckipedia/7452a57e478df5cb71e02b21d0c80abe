### Keybase proof

I hereby claim:

  * I am puckipedia on github.
  * I am puckipedia (https://keybase.io/puckipedia) on keybase.
  * I have a public key ASCl-0has7BFn1YzHPCp0A059Mgj9slSl9xCZnxz5zMm8wo

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0101a0b114f12db4e64cca34b99bd5486212e351bba7ec8f77310c95f3aa6aa6e3710a",
      "host": "keybase.io",
      "kid": "0120a5fb485ab3b0459f56331cf0a9d00d39f4c823f6c95297dc42667c73e73326f30a",
      "uid": "c497eb586d68eb31da382a66c2cc8b19",
      "username": "puckipedia"
    },
    "merkle_root": {
      "ctime": 1524142680,
      "hash": "b7f51263c813b44497f71b12649177eaeb35df6cb266493bb76c44e5e1fe979b267e219ce883e6b4f0c150e0de0b558571126c220b71295e36e7f9f3f2baf6a6",
      "hash_meta": "de42c8ae20249ffba25c3450e2a0b5b34d4920a9eecff5ae58ef34d58cbb8969",
      "seqno": 2412404
    },
    "service": {
      "entropy": "ZgS2K4PJBLQj2V3Dc/LWET+w",
      "name": "github",
      "username": "puckipedia"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "1.0.47"
  },
  "ctime": 1524142690,
  "expire_in": 504576000,
  "prev": "dd5817db7d64f10b2864ff02e77fe6675e1711476fef9d2db2ae6fcb72073afe",
  "seqno": 20,
  "tag": "signature"
}
```

with the key [ASCl-0has7BFn1YzHPCp0A059Mgj9slSl9xCZnxz5zMm8wo](https://keybase.io/puckipedia), yielding the signature:

```
hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgpftIWrOwRZ9WMxzwqdANOfTII/bJUpfcQmZ8c+czJvMKp3BheWxvYWTESpcCFMQg3VgX231k8QsoZP8C53/mZ14XEUdv750tsq5vy3IHOv7EIKxfWrlknJfXkKCXcpGQOisx4OXwWcHhb8iSuJLsu1kNAgHCo3NpZ8RAJ/7DeTRq0zGq3pG3nMwK9jCF3X8u5eXTdEuO511BIvcupm61i/7MkJm4C3uM8fQ9FzYvkRr2NmQygYcCo5bjD6hzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIGXuTL1sNoDP73HLIOcM0oJqqyqHyouVRcEYlmpaKSTYo3RhZ80CAqd2ZXJzaW9uAQ==

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/puckipedia

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id puckipedia
```